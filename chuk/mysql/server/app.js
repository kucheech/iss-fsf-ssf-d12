const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const mysql = require("mysql");

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const NODE_PORT = process.env.PORT || 3000;

const pool = mysql.createPool({
    host: "localhost",
    port: 3306,
    user: "fred",
    password: "password",
    database: "sakila",
    connectionLimit: 4
});

const handleError = function (err, res) {
    res.status(500);
    res.type("text/plain");
    res.send(JSON.stringify(err));
}


const returnResult = function (records, res) {
    res.status(200);
    res.type("text/html");
    var result = "<ol>";
    for (var i in records) {
        result += "<li>" + records[i].title + "</li>";
    }
    result += "</ol>";
    res.send(result);
}

const returnCount = function (records, res) {
    res.status(200);
    res.type("text/plain");
    var result = "count: " + records[0].n;
    res.send(result);
}

app.get("/films", function (req, res) {
    pool.getConnection(function (err, conn) {
        if (err) {
            console.error("Error: " + err);
            handleError(err, res);
            conn.release();
            return;
        }

        const QUERY = "select * from film limit 100";
        conn.query(QUERY, function (err, result) {
            if (err) {
                console.error("Error: " + err);
                conn.release();
                return;
            }

            returnResult(result, res);

            conn.release();
        });
    });
});

app.get("/films/count", function (req, res) {
    pool.getConnection(function (err, conn) {
        if (err) {
            console.error("Error: " + err);
            handleError(err, res);
            conn.release();
            return;
        }

        const QUERY = "select count(*) as n from film";
        conn.query(QUERY, function (err, result) {
            if (err) {
                console.error("Error: " + err);
                conn.release();
                return;
            }

            returnCount(result, res);

            conn.release();
        });
    });
});

const CLIENT_FOLDER = path.join(__dirname, "/../client/");
const LIBS_FOLDER = path.join(__dirname, "/../client/bower_components")
app.use("/", express.static(CLIENT_FOLDER));
app.use("/libs", express.static(LIBS_FOLDER));

app.get("/hello", function (req, res) {
    res.status(200).send("hello back");
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

//make the app public. In this case, make it available for the testing platform
module.exports = app